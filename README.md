<div align="center"><img width="40%" src="./images/logo.png" /></div>

-------------------------------------------------------------------------------

<div align="center">

![](https://img.shields.io/badge/pattern-MVC-blue)
![](https://img.shields.io/badge/PHP-8.2.12-darkblue)
![](https://img.shields.io/badge/Symfony-vesion%205.8.2-darkgreen)
![](https://img.shields.io/badge/Symfony%20version-stable-blue)
![](https://img.shields.io/badge/contact-dr.mokira%40gmail.com-blueviolet)

</div>

Une plateforme web robuste développée pour la gestion complète des
événements universitaires, favorisant l'interaction et la participation de la communauté
étudiante.



## Objectifs

- Création d'un calendrier interactif permettant la visualisation des événements à venir.
- Mise en place d'un système de création d'événements avec des pages détaillées.
- Facilitation de l'inscription en ligne pour les étudiants intéressés.
- Intégration d'un système de notation et de commentaires pour chaque événement.
- Développement d'un tableau de bord personnalisé pour les organisateurs d'événements.
- Mise en place de notifications pour informer les utilisateurs des mises à jour et des nouveaux
événements.


## Échéancier

- **Phase 1 :** Développement du calendrier interactif et du système de création d'événements.
- **Phase 2 :** Intégration du système d'inscription en ligne et de la promotion.
- **Phase 3 :** Ajout du système de notation, de commentaires et du tableau de bord.
- **Phase 4 :** Implémentation des notifications et finalisation du projet.


## Technologies Utilisées

- Symfony pour le développement back-end.
- Base de données MySQL pour le stockage des informations.
- HTML, CSS, et JavaScript pour le développement front-end.
- Intégration d'APIs pour les fonctionnalités de partage sur les réseaux sociaux.

# Documentation

## Environnement de développement
Pour créer la base de données, après avoir créer et configurer le fichier `.env.dev.local`, avec

```
DATABASE_URL="mysql://nom_utilisaeur:mot_de_passe@127.0.0.1:3306/nomDeMabase?serverVersion=8.0.32&charset=utf8mb4"
```

il faut tout simplement exécuter pour créer la base de données :

```sh
php bin/console doctrine:database:create
```

Ensuite pour créer les tables dans la base de données
il faut tout simplement exécuter les deux commandes suivantes :

1. Pour exécuter les migrations des entités dans la base de données, il faut exécuter
certaines commandes.

```sh
php bin/console make:migration
```

2. Pour appliquer ces migrations dans la base de données, il faut exécuter la commandes doctrine
suivante :


```sh
php bin/console doctrine:migrations:migrate
```

> A chaque fois que vous ajoutez une nouvelle entité ou modifiez les attributs d'une entité existante,
il faut toujours reexécuter ces deux commandes. Sinon, exécuter simplement la seconde commande
qui est celle de doctrine.

Effacer les caches avant de démarrer le serveur de développement.

```sh
php bin/console cache:clear
```

Pour démarrer le serveur de développement, il faut ce placer dans le dossier racine du projet,
ensuite ouvrir un terminal dans ce dossier, et enfin exécuter la commande suivante dans le
terminal.

```sh
symfony server:start
```

Par défaut, l'application sera accessible sur ce lien :
[http://127.0.0.1:8000](http://127.0.0.1:8000)
ou ce lien [http://localhost:8000](http://localhost:8000).

# Références

1. **Création de la relation** `ManyToOne` [https://symfony.com/doc/current/doctrine/associations.html#mapping-the-manytoone-relationship](https://symfony.com/doc/current/doctrine/associations.html#mapping-the-manytoone-relationship);
2. **Diffusion d'entité** (Add the ability to broadcast entity updates using Symfony UX Turbo?) [https://symfonycasts.com/screencast/turbo/entity-broadcast](https://symfonycasts.com/screencast/turbo/entity-broadcast);
