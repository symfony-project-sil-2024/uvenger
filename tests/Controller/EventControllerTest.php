<?php

namespace App\Test\Controller;

use App\Entity\Event;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EventControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private EntityRepository $repository;
    private string $path = '/event/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = static::getContainer()->get('doctrine')->getManager();
        $this->repository = $this->manager->getRepository(Event::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Event index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'event[subject]' => 'Testing',
            'event[objective]' => 'Testing',
            'event[startDt]' => 'Testing',
            'event[endDt]' => 'Testing',
            'event[location]' => 'Testing',
            'event[eventAnimatorsDetails]' => 'Testing',
            'event[description]' => 'Testing',
            'event[image]' => 'Testing',
            'event[published]' => 'Testing',
            'event[organizers]' => 'Testing',
            'event[category]' => 'Testing',
        ]);

        self::assertResponseRedirects('/sweet/food/');

        self::assertSame(1, $this->getRepository()->count([]));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new Event();
        $fixture->setSubject('My Title');
        $fixture->setObjective('My Title');
        $fixture->setStartDt('My Title');
        $fixture->setEndDt('My Title');
        $fixture->setLocation('My Title');
        $fixture->setEventAnimatorsDetails('My Title');
        $fixture->setDescription('My Title');
        $fixture->setImage('My Title');
        $fixture->setPublished('My Title');
        $fixture->setOrganizers('My Title');
        $fixture->setCategory('My Title');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Event');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new Event();
        $fixture->setSubject('Value');
        $fixture->setObjective('Value');
        $fixture->setStartDt('Value');
        $fixture->setEndDt('Value');
        $fixture->setLocation('Value');
        $fixture->setEventAnimatorsDetails('Value');
        $fixture->setDescription('Value');
        $fixture->setImage('Value');
        $fixture->setPublished('Value');
        $fixture->setOrganizers('Value');
        $fixture->setCategory('Value');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'event[subject]' => 'Something New',
            'event[objective]' => 'Something New',
            'event[startDt]' => 'Something New',
            'event[endDt]' => 'Something New',
            'event[location]' => 'Something New',
            'event[eventAnimatorsDetails]' => 'Something New',
            'event[description]' => 'Something New',
            'event[image]' => 'Something New',
            'event[published]' => 'Something New',
            'event[organizers]' => 'Something New',
            'event[category]' => 'Something New',
        ]);

        self::assertResponseRedirects('/event/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getSubject());
        self::assertSame('Something New', $fixture[0]->getObjective());
        self::assertSame('Something New', $fixture[0]->getStartDt());
        self::assertSame('Something New', $fixture[0]->getEndDt());
        self::assertSame('Something New', $fixture[0]->getLocation());
        self::assertSame('Something New', $fixture[0]->getEventAnimatorsDetails());
        self::assertSame('Something New', $fixture[0]->getDescription());
        self::assertSame('Something New', $fixture[0]->getImage());
        self::assertSame('Something New', $fixture[0]->getPublished());
        self::assertSame('Something New', $fixture[0]->getOrganizers());
        self::assertSame('Something New', $fixture[0]->getCategory());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();
        $fixture = new Event();
        $fixture->setSubject('Value');
        $fixture->setObjective('Value');
        $fixture->setStartDt('Value');
        $fixture->setEndDt('Value');
        $fixture->setLocation('Value');
        $fixture->setEventAnimatorsDetails('Value');
        $fixture->setDescription('Value');
        $fixture->setImage('Value');
        $fixture->setPublished('Value');
        $fixture->setOrganizers('Value');
        $fixture->setCategory('Value');

        $this->manager->remove($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/event/');
        self::assertSame(0, $this->repository->count([]));
    }
}
