<?php

namespace App\Controller;

use App\Entity\ProfessionalStatus;
use App\Form\ProfessionalStatusType;
use App\Repository\ProfessionalStatusRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/professional/status')]
class ProfessionalStatusController extends AbstractController
{
    #[Route('/', name: 'app_professional_status_index', methods: ['GET'])]
    public function index(ProfessionalStatusRepository $professionalStatusRepository): Response
    {
        return $this->render('professional_status/index.html.twig', [
            'professional_statuses' => $professionalStatusRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_professional_status_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $professionalStatus = new ProfessionalStatus();
        $form = $this->createForm(ProfessionalStatusType::class, $professionalStatus);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($professionalStatus);
            $entityManager->flush();

            return $this->redirectToRoute('app_professional_status_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('professional_status/new.html.twig', [
            'professional_status' => $professionalStatus,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_professional_status_show', methods: ['GET'])]
    public function show(ProfessionalStatus $professionalStatus): Response
    {
        return $this->render('professional_status/show.html.twig', [
            'professional_status' => $professionalStatus,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_professional_status_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ProfessionalStatus $professionalStatus, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ProfessionalStatusType::class, $professionalStatus);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_professional_status_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('professional_status/edit.html.twig', [
            'professional_status' => $professionalStatus,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_professional_status_delete', methods: ['POST'])]
    public function delete(Request $request, ProfessionalStatus $professionalStatus, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$professionalStatus->getId(), $request->request->get('_token'))) {
            $entityManager->remove($professionalStatus);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_professional_status_index', [], Response::HTTP_SEE_OTHER);
    }
}
