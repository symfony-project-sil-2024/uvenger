<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Event;
use App\Entity\Organizer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('subject')
            ->add('objective')
            ->add('startDt')
            ->add('endDt')
            ->add('location')
            ->add('eventAnimatorsDetails')
            ->add('description')
            ->add('image')
            ->add('published')
            ->add('organizers', EntityType::class, [
                'class' => Organizer::class,
'choice_label' => 'id',
'multiple' => true,
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
'choice_label' => 'id',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
        ]);
    }
}
